#pragma checksum "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Download.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c8e3ad172ea1bf655037fbd5663ee7ecc52de4dd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FytSoa.Web.Pages.FytAdmin.Cms.Pages_FytAdmin_Cms_Download), @"mvc.1.0.razor-page", @"/Pages/FytAdmin/Cms/Download.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/FytAdmin/Cms/Download.cshtml", typeof(FytSoa.Web.Pages.FytAdmin.Cms.Pages_FytAdmin_Cms_Download), null)]
namespace FytSoa.Web.Pages.FytAdmin.Cms
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\_ViewImports.cshtml"
using FytSoa.Web;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c8e3ad172ea1bf655037fbd5663ee7ecc52de4dd", @"/Pages/FytAdmin/Cms/Download.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21c44af9864cf57cf01e8fd1fe389a8e352e148c", @"/Pages/_ViewImports.cshtml")]
    public class Pages_FytAdmin_Cms_Download : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsTop", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsComment", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsLink", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Download.cshtml"
  
    ViewData["Title"] = "下载管理";
    Layout = AdminLayout.Pjax(HttpContext);

#line default
#line hidden
            BeginContext(144, 366, true);
            WriteLiteral(@"<div id=""content-body"">
    <div class=""list-wall"">
        <div class=""layui-form list-search"">
            <div class=""layui-inline"">
                <input class=""layui-input"" id=""key"" autocomplete=""off"" placeholder=""请输入关键字查询"">
            </div>
            <div class=""layui-inline"">
                <select id=""attr"" lay-search="""">
                    ");
            EndContext();
            BeginContext(510, 28, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "003492c1fa914cfdaac3e260a53c7439", async() => {
                BeginContext(527, 2, true);
                WriteLiteral("全部");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(538, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(560, 33, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d55a05dfacda4528b9abad5c778370b3", async() => {
                BeginContext(582, 2, true);
                WriteLiteral("推荐");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(593, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(615, 39, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b7dedca20514c1a83dca6018ee8c79a", async() => {
                BeginContext(641, 4, true);
                WriteLiteral("是否评论");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(654, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(676, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9da8bcb83c084b5b802c51610b3bb94d", async() => {
                BeginContext(699, 4, true);
                WriteLiteral("下载外链");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(712, 3189, true);
            WriteLiteral(@"
                </select>
            </div>
            <div class=""layui-inline"" style=""width:100px;"">
                <input type=""checkbox"" checked=""checked"" name=""close"" lay-filter=""switch"" lay-skin=""switch"" lay-text=""已审核|未审核"">
            </div>
            <button type=""button"" class=""layui-btn layui-btn-sm"" data-type=""toolSearch""><i class=""layui-icon layui-icon-search""></i> 搜索</button>
        </div>
        <table class=""layui-hide"" id=""tablist"" lay-filter=""tool""></table>
    </div>
    <script type=""text/html"" id=""toolbar"">
        <div class=""layui-btn-container"">
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""toolAdd""><i class=""layui-icon""></i> 新增</button>
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""toolDel""><i class=""layui-icon""></i> 删除</button>
        </div>
    </script>
    <input type=""hidden"" id=""isSave"" value=""0"" />
    <script type=""text/html"" id=""switchTpl"">
        <input type=""checkbox"" name=""status"" valu");
            WriteLiteral(@"e=""{{d.id}}"" lay-skin=""switch"" disabled lay-text=""已审核|未审核"" lay-filter=""statusedit"" {{ d.audit==1?'checked':''}}>
    </script>
    <script>
        var active, layuiIndex;
        layui.config({
            base: '/themes/js/modules/'
        }).use(['table', 'layer', 'jquery', 'common', 'form'],
            function () {
                var table = layui.table,
                    layer = layui.layer,
                    $ = layui.jquery,
                    os = layui.common,
                    form = layui.form;
                form.render();
                table.render({
                    elem: '#tablist',
                    toolbar: '#toolbar',
                    headers: os.getToken(),
                    url: '/api/download/getpages',
                    cols: [
                        [
                            { type: 'checkbox', fixed: 'left' },
                            {
                                field: 'title', title: '标题', fixed: 'left', templet: function ");
            WriteLiteral(@"(res) {
                                    return '<a href=""javascript:void(0)"" lay-event=""edit"" class=""text-color"">' + res.title + '</a>';
                                }
                            },
                            {
                                field: 'attr', width: 200, title: '属性', templet: function (res) {
                                    return attrHtml(res);
                                }
                            },
                            { field: 'fileType', width: 100, title: '文件类型' },
                            { field: 'fileSize', width: 100, title: '文件大小' },
                            { field: 'audit', width: 120, title: '审核状态', templet: '#switchTpl' },
                            { field: 'downSum', width: 100, title: '下载量', sort: true },
                            { field: 'hits', width: 100, title: '点击量' },
                            { field: 'editDate', width: 200, title: '更新时间' }
                        ]
                    ],
         ");
            WriteLiteral("           page: true,\r\n                    id: \'tables\',\r\n                    where: {\r\n                        id:\'");
            EndContext();
            BeginContext(3902, 14, false);
#line 79 "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Download.cshtml"
                       Write(Model.ColumnId);

#line default
#line hidden
            EndContext();
            BeginContext(3916, 447, true);
            WriteLiteral(@"'
                    }
                });

                var type = 0, selStr = '';
                active = {
                    reload: function () {
                        table.reload('tables',
                            {
                                page: {
                                    curr: 1
                                },
                                where: {
                                    id:'");
            EndContext();
            BeginContext(4364, 14, false);
#line 92 "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Download.cshtml"
                                   Write(Model.ColumnId);

#line default
#line hidden
            EndContext();
            BeginContext(4378, 476, true);
            WriteLiteral(@"',
                                    where: $(""#attr"").val(),
                                    key: $('#key').val(),
                                    audit: type
                                },
                                done: function () {
                                }
                            });
                    },
                    //添加栏目
                    toolAdd: function () {
                        active.goModify('?column=");
            EndContext();
            BeginContext(4855, 14, false);
#line 103 "E:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Download.cshtml"
                                            Write(Model.ColumnId);

#line default
#line hidden
            EndContext();
            BeginContext(4869, 3885, true);
            WriteLiteral(@"');
                    },
                    goModify: function (parm = '') {
                        var winH = $(window).height(), winW = $(window).width();
                        layuiIndex = os.OpenRight('下载管理', ""/fytadmin/cms/downmodify"" + parm, winW - 220 + 'px', winH - 61 + 'px', function () {
                            if (parseInt($(""#isSave"").val()) === 1) {
                                $(""#isSave"").val('0');
                                active.reload();
                            }
                        }, function () {
                            active.closeCloumnModify();
                        });
                    },
                    closeCloumnModify: function () {
                        var $layero = $('#layui-layer' + layuiIndex);
                        $layero.animate({
                            left: $layero.offset().left + $layero.width()
                        }, 300, function () {
                            layer.close(layuiIndex);
          ");
            WriteLiteral(@"              });
                        return false;
                    },
                    toolSearch: function () {
                        active.reload();
                    },
                    toolDel: function () {
                        var checkStatus = table.checkStatus('tables')
                            , data = checkStatus.data;
                        if (data.length === 0) {
                            os.error(""请选择要删除的项目~"");
                            return;
                        }
                        var str = '';
                        $.each(data, function (i, item) {
                            str += item.id + "","";
                        });
                        layer.confirm('确定要执行批量删除吗？', function (index) {
                            layer.close(index);
                            var loadindex = layer.load(1, {
                                shade: [0.1, '#000']
                            });
                            os.ajax('api/do");
            WriteLiteral(@"wnload/delete/', { parm: str }, function (res) {
                                layer.close(loadindex);
                                if (res.statusCode === 200) {
                                    active.reload();
                                    os.success('删除成功！');
                                } else {
                                    os.error(res.message);
                                }
                            });
                        });
                    }
                };
                table.on('toolbar(tool)', function (obj) {
                    active[obj.event] ? active[obj.event].call(this) : '';
                });
                $('.list-search .layui-btn').on('click', function () {
                    var type = $(this).data('type');
                    active[type] ? active[type].call(this) : '';
                });
                //监听指定开关
                form.on('switch(switch)', function (data) {
                    type = this.checked ? 0");
            WriteLiteral(@" : 1;
                });
                //监听工具条
                table.on('tool(tool)', function (obj) {
                    var data = obj.data;
                    if (obj.event === 'edit') {
                        active.goModify('?id=' + data.id);
                    }
                });
                
            });
        function attrHtml(e) {
            var h = '';
            if (e.isTop) {
                h += '<span class=""layui-badge layui-bg-cyan"">推荐</span>';
            }
            if (e.isComment) {
                h += '<span class=""layui-badge layui-bg-cyan"">评论</span>';
            }
            if (e.IsLink) {
                h += '<span class=""layui-badge layui-bg-cyan"">下载外链</span>';
            }
            return h;
        }
    </script>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FytSoa.Web.Pages.FytAdmin.Cms.DownloadModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FytSoa.Web.Pages.FytAdmin.Cms.DownloadModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FytSoa.Web.Pages.FytAdmin.Cms.DownloadModel>)PageContext?.ViewData;
        public FytSoa.Web.Pages.FytAdmin.Cms.DownloadModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
